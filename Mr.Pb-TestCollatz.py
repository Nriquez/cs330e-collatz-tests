#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve



# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 900)
        self.assertEqual(j, 1000)

    def test_read_4(self):
        s = "25000 26000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 25000)
        self.assertEqual(j, 26000)

    def test_read_5(self):
        s = "50000 51000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 50000)
        self.assertEqual(j, 51000)

    def test_read_6(self):
        s = "999000 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999000)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(25000, 26000)
        self.assertEqual(v, 264)

    def test_eval_6(self):
        v = collatz_eval(50000, 51000)
        self.assertEqual(v, 278)

    def test_eval_7(self):
        v = collatz_eval(75000, 76000)
        self.assertEqual(v, 307)

    def test_eval_8(self):
        v = collatz_eval(999000, 999999)
        self.assertEqual(v, 396)

    def test_eval_9(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_10(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_11(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 50000, 51000, 278)
        self.assertEqual(w.getvalue(), "50000 51000 278\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 999000, 999999, 396)
        self.assertEqual(w.getvalue(), "999000 999999 396\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("25000 26000\n50000 51000\n75000 76000\n999000 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "25000 26000 264\n50000 51000 278\n75000 76000 307\n999000 999999 396\n")

    def test_solve_3(self):
        r = StringIO("1 1\n2 2\n3 3\n4 4\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 2 2\n3 3 8\n4 4 3\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
