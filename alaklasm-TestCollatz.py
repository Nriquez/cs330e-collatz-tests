#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_4(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(100, 5000)
        self.assertEqual(v, 238)

    def test_eval_7(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)

    def test_eval_8(self):
        v = collatz_eval(700000, 800000)
        self.assertEqual(v, 504)

    def test_eval_9(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_10(self):
        v = collatz_eval(1, 5000)
        self.assertEqual(v, 238)

    def test_eval_11(self):
        v = collatz_eval(388, 388)
        self.assertEqual(v, 121)

    def test_eval_12(self):
        v = collatz_eval(1, 200000)
        self.assertEqual(v, 383)

    def test_eval_13(self):
        v = collatz_eval(800000, 700000)
        self.assertEqual(v, 504)

    def test_eval_14(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_15(self):
        v = collatz_eval(9, 1000)
        self.assertEqual(v, 179)

    def test_eval_16(self):
        v = collatz_eval(1387, 2002)
        self.assertEqual(v, 180)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1387, 2002, 180)
        self.assertEqual(w.getvalue(), "1387 2002 180\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 9, 1000, 179)
        self.assertEqual(w.getvalue(), "9 1000 179\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 800000, 700000, 504)
        self.assertEqual(w.getvalue(), "800000 700000 504\n")

    # -----
    # solve some test cases
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("700000 800000\n100 5000\n10 1\n1 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "700000 800000 504\n100 5000 238\n10 1 20\n1 2 2\n")

    def test_solve_3(self):
        r = StringIO("388 388\n1 200000\n9 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "388 388 121\n1 200000 383\n9 1000 179\n")

    def test_solve_4(self):
        r = StringIO("1 1\n900 1000\n201 210\n100 200\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n900 1000 174\n201 210 89\n100 200 125\n")

# ----
# main
# ----


# pragma: no cover
if __name__ == "__main__":
    main()


""" 
#pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1
$ cat TestCollatz.out
............................
----------------------------------------------------------------------
Ran 28 tests in 9.676s

OK


$ coverage report -m                   >> TestCollatz.out
$ cat TestCollatz.out
............................
----------------------------------------------------------------------
Ran 28 tests in 9.676s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          32      0     14      0   100%
TestCollatz.py     110      0      0      0   100%
------------------------------------------------------------
TOTAL              142      0     14      0   100%
"""
